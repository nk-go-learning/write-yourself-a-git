package utils

import "os"

// WriteStringFile writes a string to the filepath
func WriteStringFile(filepath, content string) error {
	file, err := os.OpenFile(filepath, os.O_RDWR|os.O_CREATE, 0666)
	defer file.Close()

	if err != nil {
		return err
	}

	file.WriteString(content)
	file.Sync()

	return nil
}
