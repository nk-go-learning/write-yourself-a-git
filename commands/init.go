package commands

import (
	"fmt"
	"log"
	"os"
	"write-yourself-a-git/repository"
)

// Init "git init" command
func Init(worktree string) {
	dir := getWorkingDirectory(worktree)

	if git, err := repository.GetGitRepository(dir, false); err != nil {
		repo, err := repository.InitNewRepository(dir)

		if err != nil {
			log.Fatalf("Cannot init new repository: %s", err)
		}

		fmt.Println(repo)
	} else {
		fmt.Println(git)
	}
}

// getWorkingDirectory gets the working directory or the current directory
func getWorkingDirectory(worktree string) string {
	var dir string = worktree

	if worktree == "" {
		dir, err := os.Getwd()

		if err != nil {
			log.Fatal(err)
		}

		worktree = dir
	}

	return dir
}
