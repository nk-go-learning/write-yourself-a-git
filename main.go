package main

import (
	"log"
	"os"
	cmd "write-yourself-a-git/commands"
)

func main() {
	log.Println(os.Args)

	args := os.Args[2:]

	if len(args) > 0 {
		command := args[0]

		switch command {
		case "init":
			if len(args) > 1 {
				cmd.Init(args[1])
			} else {
				cmd.Init("")
			}
		default:
			log.Println("Unrecognised command")
		}
	} else {
		log.Println("No commands supplied")
	}
}
