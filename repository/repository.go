package repository

import (
	"fmt"
	"os"
	"path"
	"write-yourself-a-git/utils"
)

// GitRepository struct
type GitRepository struct {
	worktree string
	gitdir   string
	config   *GitConfig
}

// GetGitRepository creates a new GitRepository struct
func GetGitRepository(worktree string, force bool) (*GitRepository, error) {

	gitdir := path.Join(worktree, ".git")

	if _, err := os.Stat(gitdir); os.IsNotExist(err) {
		return nil, fmt.Errorf("Not a Git repository %s", worktree)
	}

	config, err := GetConfig(gitdir)

	if err != nil {
		return nil, err
	}

	return &GitRepository{
		worktree: worktree,
		gitdir:   gitdir,
		config:   config,
	}, nil
}

// InitNewRepository inits a new repository inside the working directory
func InitNewRepository(worktree string) (*GitRepository, error) {

	// Check to see if the supplied path exists, if not, create it
	if _, err := os.Stat(worktree); os.IsNotExist(err) {
		err := os.MkdirAll(worktree, 0777)

		if err != nil {
			return nil, err
		}
	}

	// Create all required git files

	gitdir := path.Join(worktree, ".git")
	os.Mkdir(gitdir, 0777)

	os.Mkdir(path.Join(gitdir, "branches"), 0777)
	os.Mkdir(path.Join(gitdir, "objects"), 0777)
	os.Mkdir(path.Join(gitdir, "refs"), 0777)
	os.Mkdir(path.Join(gitdir, "refs", "tags"), 0777)
	os.Mkdir(path.Join(gitdir, "refs", "heads"), 0777)

	utils.WriteStringFile(path.Join(gitdir, "description"), "Unnamed repository; edit this file 'description' to name the repository.\n")
	utils.WriteStringFile(path.Join(gitdir, "HEAD"), "ref: refs/heads/master\n")

	GenerateConfigFile(gitdir)

	return GetGitRepository(worktree, false)
}
