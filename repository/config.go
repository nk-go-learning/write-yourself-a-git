package repository

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"write-yourself-a-git/utils"
)

// GitConfig struct
type GitConfig struct {
	file   *os.File
	config string
}

// GetConfig gets or creates the git config file
func GetConfig(gitdir string) (*GitConfig, error) {
	configPath := path.Join(gitdir, "config")
	configFile, err := os.OpenFile(configPath, os.O_RDWR|os.O_CREATE, 0666)

	if err != nil {
		return nil, fmt.Errorf("Cannot create .git config file: %s", err)
	}

	bytes, err := ioutil.ReadFile(configPath)
	configContents := string(bytes)

	return &GitConfig{
		file:   configFile,
		config: configContents,
	}, nil
}

// GenerateConfigFile creates the initial config file
func GenerateConfigFile(gitdir string) error {
	configPath := path.Join(gitdir, "config")

	content := "repositoryformatversion=0\n" +
		"filemode=false\n" +
		"bare=false\n"

	err := utils.WriteStringFile(configPath, content)

	if err != nil {
		return err
	}

	return nil
}
